
const Sequelize = require('sequelize');
  config = require('./config');

const sequelize = new Sequelize('mosaic', 'root', '', {
	dialect: "mariadb", // or 'sqlite', 'postgres', 'mariadb'
	port:    3306,
});

var User = sequelize.define('user', {
  name: Sequelize.STRING
});

var Movie = sequelize.define('movie', {
  name: Sequelize.STRING
});

module.exports = exports = {
  sequelize,
  User,
  Movie
};

